(ns user
  (:require [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.pprint :refer [pprint]]
            [clojure.repl :refer :all]
            [clojure.test :as test]
            [clojure.tools.namespace.repl :refer [refresh refresh-all]]
            [clojure-workflow.core :as system]))

(def system {})

(defn stop
  "Shuts down and destroys the current development system."
  []
  (alter-var-root #'system
                  (fn [s] (when s (system/stop s)))))

(defn start
  "Initializes the current development system and starts it running."
  []
  (alter-var-root #'system
                  (fn [s] (when s (system/start s)))))

(defn reset []
  (stop)
  (refresh :after 'user/start))