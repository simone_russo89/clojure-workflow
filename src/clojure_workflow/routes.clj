(ns clojure-workflow.routes
  (:use compojure.core)
  (:require [ring.adapter.jetty :as jetty]
            [compojure.route :as route]))

(defroutes app-routes
           (GET "/" [] "Hello world!")
           (route/not-found "Not Found"))

(defn start-webapp []
  (jetty/run-jetty app-routes {:join? false :port 3000}))

(defn stop-webapp [webapp]
  (.stop webapp))