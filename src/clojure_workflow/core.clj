(ns clojure-workflow.core
  (:require [clojure-workflow.routes :refer [start-webapp stop-webapp]]))

(defn system
  "Returns a new instance of the whole application.
  Does not have any side effects."
  []
  {})

(defn start
  "Performs side effects to initialize the system, acquire resources,
  and start it running. Returns an updated instance of the system."
  [system]
  (println "Starting application")
  (let [system {:webapp (start-webapp)}]
    (println "Started application")
    system))

(defn stop
  "Performs side effects to shut down the system and release its
  resources. Returns an updated instance of the system."
  [system]
  (println "Stopping application")
  (let [webapp (:webapp system)]
    (when webapp
      (stop-webapp (:webapp system))))
  (println "Stopped application")
  system)